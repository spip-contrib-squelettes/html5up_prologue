# HTML5up Prologue pour SPIP

Adaptation pour SPIP du squelette «Escape Prologue» de html5up https://html5up.net/prologue/

Le thème est prévu pour des sites simples, soit avec une seul niveau de rubrique (mode site), soit avec une seule rubrique (mode blog), soit avec uniquement une galerie d'images (mode portfolio).

## Documentation

https://contrib.spip.net/5515