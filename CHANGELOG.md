# Changelog

## 0.1.4 - 2025-02-21

### Fixed

- Ne pas afficher l'entrée contact dans le menu si le plugin Formulaire de contact avancé n'est pas actif
- Utiliser le bonne police
- Ne pas afficher le lien entourant l'image d'entête s'il n'y a pas d'image d'entête dans l'article mis en avant sur l'accueil
- a11y : Augmenter le contraste du texte par rapport au fond pour une meilleure accessibilité
- Ajouter de la marge sous les intertitres dans le contenu des articles
- Ajouter la classe `.contenu` à l'article mis en avant sur l'accueil
- Meilleur affichage du champ d'inscription à la newsletter dans le formulaire de contact
- Sélectionner le nombre d'items (6) par défaut la grille sur la page de configuration du thème
- Réparer la boucle année dans le pied de page
- Ménage dans les classes du menu de pied de page

### Changed

- Utiliser des `<inclure>` pour les sections sections de la page d'accueil

## 0.1.3 - 2024-01-13

### Fixed

- Faire fonctionner le menu si le site est installé dans un sous dossier

## 0.1.2 - 2024-01-04

### Fixed

- Ajustement des marges et centrage des textes dans les grilles
- Limiter la longueur des titres dans les items de la grille pour n'avoir qu'une ligne

### Changed

- afficher toutes les rubriques, articles ou images dans la grille de l'accueil avec une pagination définie par le nombre d'items choisi dans la configuration.

## 0.1.1 - 2024-01-03

### Fixed

- Réparer le chemin vers l'image overlay quand c'est possible ou la supprimer si ce n'est pas indispensable
- Meilleur affichage des entrées du menu quand elles sont longues
- #8 Afficher le titre par défaut de la grille si ce titre a été personnalisé puis supprimé
- #3 Réparer le picto burger du menu sur petit écran
- Faire fonctionner le menu sur les autres pages que l'accueil
- Meilleur affichage des grilles sur petit écran
- faire fonctionner les grilles (accueil, rubrique) avec 15 items maximum

### Featured

- Ajout des modèles `<bouton>` et `<icône>`
- Pouvoir personnaliser le titre de la grille d'accueil
- Pouvoir choisir le nombre d'items de la grille d'accueil

### Changed

- Donner un identifiant générique à la section grille car ce n'est pas nécessairement un portfolio
- Utiliser le plugin Ancres douces au lieu du script original du thème pour des questions de pérennité.

## 0.1.0 - 2023-12-28

### Featured

- Sortie de la 1ère version en développement