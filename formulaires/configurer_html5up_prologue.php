<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Formulaire de configuration,
 * déclaration des saisies.
 * @return array
 **/
function formulaires_configurer_html5up_prologue_saisies_dist(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		'options' => [
			// Changer l'intitulé du bouton final de validation
			'texte_submit' => '<:bouton_enregistrer:>'
		],
		[ // Accueil
		'saisie' => 'fieldset',
			'options' => [
				'nom' => 'accueil',
				'label' => '<:public:accueil_site:>'
			],
			'saisies' => [
				 // Visuel d'entête de l'accueil
				['saisie' => 'selecteur_document',
					'options' => [
						'nom' => 'accueil_visuel_entete',
						'label' => '<:html5up_prologue:accueil_visuel_entete:>',
						'explication' => '<:html5up_prologue:accueil_visuel_entete_explication:>'
					]
				],
				// Type d'affichage de la grille d'accueil
				['saisie' => 'radio',
					'options' => [
						'nom' => 'accueil_mode',
						'label' => '<:html5up_prologue:accueil_mode:>',
						'explication' => '<:html5up_prologue:accueil_mode_explication:>',
						'data' => [
							'site' => '<:html5up_prologue:accueil_mode_site:>',
							'blog' => '<:html5up_prologue:accueil_mode_blog:>',
							'portfolio' => '<:html5up_prologue:accueil_mode_portfolio:>',
						],
						'defaut' => 'site',
					]
				],
				// Titre de la grille d'accueil
				['saisie' => 'input',
					'options' => [
						'nom' => 'accueil_titre_grille',
						'label' => '<:html5up_prologue:accueil_titre_grille:>',
						'explication' => '<:html5up_prologue:accueil_titre_grille_explication:>',
					]
				],
				// Nombre d'items de la grille d'accueil
				['saisie' => 'radio',
					'options' => [
						'nom' => 'accueil_nb_grille',
						'label' => '<:html5up_prologue:accueil_nb_grille:>',
						'data' => [
							'6' => '6',
							'9' => '9',
							'12' => '12',
							'15' => '15',
						],
						'defaut' => '6',
					]
				],
				// Article de l'accueil
				['saisie' => 'selecteur_article',
					'options' => [
						'nom' => 'accueil_article',
						'label' => '<:html5up_prologue:accueil_article:>',
						'explication' => '<:html5up_prologue:accueil_article_explication:>'
					]
				]
			]
		]
	];
	return $saisies;
}
